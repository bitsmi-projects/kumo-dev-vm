use std::process::{ Command, Output };

fn main() {
    let output:Output = Command::new("sh")
            .arg("-c")
            .arg("uname -m")
            .output()
            .expect("Failed to execute command");

    let result:&str = std::str::from_utf8(output.stdout.as_slice()).unwrap();


    println!("ARCH: {}", result);
}