# KUMO DEVELOPER VM

## Table of Contents

* [VM Startup](#markdown-header-vm-startup)
	* [(Optional) - Create a custom RSA key](#markdown-header-optional-create-a-custom-rsa-key)
	* [(Optional) - Disable default account](#markdown-header-optional-disable-default-account)
	* [Vagrant VM command summary](#markdown-header-vagrant-vm-command-summary)
* [Setup](#markdown-header-setup)	
* [HOW TOs](#markdown-header-how-tos)
	* [Run ARM/ARM64 embedded docker images](#markdown-header-run-armarm64-embedded-docker-images)
	* [Compile Rust code for ARM/ARM64 architectures](#markdown-header-compile-rust-code-for-armarm64-architectures)	
	* [Backup Raspberry Pi SD card](#markdown-header-backup-raspberry-pi-sd-card)	


## VM Startup

When the VM starts for first time with `vagrant up` command, it will perform the provision process described in **Ansible playbooks** located in `provision` folder.

Provision process creates an admin user account named **kumo_admin**. This user is a **sudoer** account created to group all kumo platform related tasks and permissions.
To enable it after VM first provision, stop VM with `vagrant halt` command and edit `Vagrantfile`. The following lines must be uncommented:

```
config.ssh.username = "kumo_admin"
config.ssh.private_key_path = ["id_rsa"]
```

This will enable user `kumo_admin` to logging in through SSH using default RSA key located in `id_rsa` file. **This file is not protected by a password**.

### (Optional) - Create a custom RSA key

At the moment, Vagrant RSA key only supports PEM format private keys, so `-m PEM` flag is mandatory.

```sh
ssh-keygen -m PEM -t rsa
```

It will generate 2 files: `~/.ssh/id_rsa` with the **private key** and `~/.ssh/id_rsa.pub` with the **public key**.

Copy the **public key** inside `provision` folder and the **private key** at VM root folder replacing default id_rsa file. 
Run `vagrant provision` command to copy the public key inside the VM. 

**NOTE:** On Windows systems, ssh client may not recognize private key's file as a secure source if it have too much permissions, 
E.G. if all users in the system have read / write permissions on it. To avoid these errors, ensure that only current Windows user figures in file's security policies
(Right button on file > Properties > Security)

On windows, SSH connection can be debugged with the following command (^ character is used to escape line breaks in multiple line commands):

```sh
ssh kumo_admin@127.0.0.1 -p 2222 -o LogLevel=DEBUG ^
-o Compression=yes -o DSAAuthentication=yes ^
-o IdentitiesOnly=yes -o StrictHostKeyChecking=no ^
-o UserKnownHostsFile=/dev/null -i id_rsa
```
 

### (Optional) - Disable default account

Although `kumo_admin` is the main user account designed to perform and execute all management tasks, Vagrant's default user `vagrant` it's still enabled and 
keeps all privileges of a sudoer account. It can be disabled or removed executing one of the following commands:

**Expire Account** 

It will disallow user from logging in from any source, including ssh

```sh
sudo usermod --expiredate 1 vagrant
```

Account can be reenabled setting expiration date to never o a specific date

```sh
# Set expiration to never
sudo usermod --expiredate "" vagrant

# Set expiration to an specific date formatted as YYYY-MM-DD
sudo usermod --expiredate "2020-12-31" vagrant
```

**Delete Account**

CAUTION! Deleted account cannot be restored

```sh
sudo deluser --remove-home vagrant

sudo groupdel vagrant
```

### Vagrant VM command summary

* `vagrant up`: Start VM. On first start-up, Vagrant will run the VM provision
* `vagrant halt`: Stop VM
* `vagrant reload`: Restart VM
* `vagrant provision`: Update VM provision after a change on **Vagrantfile**
* `vagrant ssh`: SSH login to VM with the default `vagrant` user or the one configured in `Vagrantfile`'s `config.ssh.username` property.

## Setup

#### Included Software

* Ansible 2.8.2
* Ansible 2.8.2
* Docker 19.03.6
* Ubuntu bionic ARM Docker image
* Ubuntu bionic ARM64 Docker image
* Debian strech-lite Docker image
* gparted

#### Network configuration

* **VM IP:** `172.16.0.10`. May be modified by a value inside any of private network ranges: 
	* 192.168.0.0 to 192.168.255.255 
	* 172.16.0.0 to 172.31.255.255 
	* 10.0.0.0 to 10.255.255.255
	
#### VM Folders
	
* `/home/kumo_admin`: **kumo_admin** user's home folder
* `/vagrant`: Shared folder corresponding to host's folder where **Vagrantfile** is located	

## HOW TOs

### Run ARM/ARM64 embedded docker images

```sh
# Ubuntu bionic ARM Docker image
sh bin/ubuntu-arm.sh

# Ubuntu bionic ARM64 Docker image
sh bin/ubuntu-arm64.sh

# Debian strech-lite Docker image
sh bin/debian-arm.sh
```

### Compile Rust code for ARM/ARM64 architectures

```sh
# ARMv7
cargo build --target=armv7-unknown-linux-gnueabihf

# ARM AARCH64
cargo build --target=aarch64-unknown-linux-gnu
```

### Backup Raspberry Pi SD card

#### Setup

This step is only needed if SD card is not accessible from inside Linux guest OS. Execute the following command and ensure that the required device is listed:

```sh
sudo fdisk -l
```

In our example, this device is `/dev/sdc`. 

If the device is not shown in the resulting list, a raw device must be configured in VirtualBox in order to access to SD card. If the device is listed, skip the following steps.

**Get the device ID of SD card reader**

On Windows host OS, Open an CMD as administrator and execute

```cmd
wmic diskdrive list brief
```

The resulting list like the following:

```cmd
D:\usr\bin\virtual_box>wmic diskdrive list brief
Caption                     DeviceID            Model                       Partitions  Size
SAMSUNG MZVLB256HAHQ-000L2  \\.\PHYSICALDRIVE1  SAMSUNG MZVLB256HAHQ-000L2  3           256052966400
ST1000LM049-2GH172          \\.\PHYSICALDRIVE0  ST1000LM049-2GH172          1           1000202273280
SDHC Card                   \\.\PHYSICALDRIVE2  SDHC Card                   2           7772889600
```

In this example, our Device ID is **\\.\PHYSICALDRIVE2**

**Create VMDK file**

Create the VMDK file which will link to the SD card, open a command windows as Administrator and execute the following command:

```cmd
D:\usr\bin\virtual_box\VBoxManage internalcommands createrawvmdk -filename "\path\to\vm\external-disk\sdcard.vmdk" -rawdisk "\\.\PHYSICALDRIVE2"
```

**Attach RAW Disk to VM**

Next attach the raw disk VMDK to guest VM within the VirtualBox UI

1. Ensure the Guest VM is not running.
2. Start VirtualBox by right-clicking on it and choosing "Run as administrator"
3. Open the settings area for the guest VM
4. Click on "Storage" in the toolbar
5. Next to the controller click on the icon to "Add Hard Disk"
6. Select "Choose existing disk"
7. Navigate to the c:/Hard Disks/sdcard.vmdk and select it
8. You should now be returned to the Storage tab and see your sdcard.vmdk in the list.

**NOTE:** In order to prevent errors on VM when SD card reader is not plugged to PC, the raw disk file must be dettached from storage section of VirtualBox after use.


#### Clone SD Card

Execute the following command from within Linux gest OS:

```sh
sudo fdisk -l
```

It may display the information of the new attached device, in this example `/dev/sdc`. To start SD clonning, execute the following command to copy
th contents of `/dev/sdc` to the destination file located in `/path/to/clone.img` file

```sh
sudo dd if=/dev/sdc of=/path/to/clone.img
```

#### Shrinking the Image

```sh
sudo pishrink.sh -d /path/to/clone.img /path/to/clone-shrink.img
```

Where:
* `-d` flag activates debug mode creatin a log file in current directory
* `/path/to/clone.img` is image file to be shrinked
* `/path/to/clone-shrink.img` is resulting shrinked file

**NOTE:** Target file location cannot be a VM host-guest shared folder such as /vagrant

#### Flash the image to a new SD Card

User [etcher](https://www.balena.io/etcher/) to write image to SD card


#### Compress the image

```sh
gzip -9 /path/to/clone-shrink.img
```
